![](assets/Bottom_up.svg)

<!--   my-icons -->
<p align="center">
    <a href="https://github.com/awadesh365"><img src="https://img.shields.io/badge/status-updating-brightgreen.svg"></a>
    <p align="left"> <img src="https://komarev.com/ghpvc/?username=awadesh365&label=Profile%20views&color=0e75b6&style=flat"
        alt="awadesh365" /> </p> 
</p>


<!--   my-header-img -->
![](./src/header_.png)
<a href="https://dev.to/crackingdemon/register-and-login-system-in-mern-stack-1n98"><img src="https://miro.medium.com/v2/resize:fit:678/1*l2tlJsFNg2tH6QizegKkqA.png" align="right" height="90" width="190" ></a>

<!--https://readme-typing-svg.herokuapp.com/demo/  -->
[![Typing SVG](http://readme-typing-svg.herokuapp.com?font=Fira+Code&pause=1000&width=435&lines=Hi+there+%F0%9F%91%8B%2C+I+am+Awadesh;Welcome+to+My+Profile;Software+Engineer;Full+Stack+Developer)](https://git.io/typing-svg)

<img src="https://www.freecodecamp.org/news/content/images/size/w2000/2023/05/pexels-tara-winstead-8386440--1-.jpg" alt ="Programing">



<!--   my-skils -->



[![LinkedIn](https://img.shields.io/badge/-Connect%20on%20LinkedIn-blue)](https://www.linkedin.com/in/awadesh-nautiyal/)

# Welcome to My Code Wonderland! 🚀

## About Me

**Software engineering is my passion, and I strive to write clean and efficient code. I'm always learning new things and keeping myself updated with the latest advancements to tackle problems in the best way possible.**

**Every line I write reflects this dedication. I'm committed to delivering high-quality solutions that meet the highest industry standards.**

**Challenges are learning opportunities. Bugs, issues, and setbacks are stepping stones to improve my skills and knowledge. Every problem is a chance to learn something new, so I embrace the struggle and keep exploring new things without fearing failure.**




# My Technical Skills


| **Category**           | **Skills**                                                                                                                                                                                                                                                                                                                                                                                                                                      |
|------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **Programming Languages** | ![Python Badge](https://img.shields.io/badge/-Python-3776AB?style=flat&logo=Python&logoColor=white) ![Java Badge](https://img.shields.io/badge/-Java-007396?style=flat&logo=Java&logoColor=white) ![C Badge](https://img.shields.io/badge/-C-00599C?style=flat&logo=C&logoColor=white) ![C++ Badge](https://img.shields.io/badge/-C++-00599C?style=flat&logo=C%2B%2B&logoColor=white)                                                                                                                                                                  |
| **Web Development**   | ![Postman Badge](https://img.shields.io/badge/-Postman-FF6C37?style=flat&logo=Postman&logoColor=white) ![Insomnia Badge](https://img.shields.io/badge/-Insomnia-5849BE?style=flat&logo=Insomnia&logoColor=white)  ![Node.js Badge](https://img.shields.io/badge/-Node.js-339933?style=flat&logo=Node.js&logoColor=white) ![Express.js Badge](https://img.shields.io/badge/-Express.js-000000?style=flat&logo=Express&logoColor=white) ![MongoDB Badge](https://img.shields.io/badge/-MongoDB-47A248?style=flat&logo=MongoDB&logoColor=white) ![React.js Badge](https://img.shields.io/badge/-React.js-61DAFB?style=flat&logo=React&logoColor=white) ![Redux Toolkit Badge](https://img.shields.io/badge/-Redux_Toolkit-764ABC?style=flat) ![Tailwind CSS Badge](https://img.shields.io/badge/-Tailwind%20CSS-38B2AC?style=flat) |
| **Backend as a Service**  | ![Firebase Badge](https://img.shields.io/badge/-Firebase-FFCA28?style=flat&logo=Firebase&logoColor=black) ![Appwrite Badge](https://img.shields.io/badge/-Appwrite-009688?style=flat&logo=Appwrite&logoColor=white)                                                                                                                                                                                                                                                                                              |
| **Blockchain**            | ![Blockchain Badge](https://img.shields.io/badge/-Blockchain-000000?style=flat&logo=Blockchain.com&logoColor=white) ![Solidity Badge](https://img.shields.io/badge/-Solidity-363636?style=flat&logo=Solidity&logoColor=white)                                                                                                                                                                                                                                                                                |                                                                                                       |
| **Data Science**          |  ![Matlab Badge](https://img.shields.io/badge/-Matlab-0076A8?style=flat&logo=Mathworks&logoColor=white) ![Data Analytics Badge](https://img.shields.io/badge/-Data%20Analytics-FF5733?style=flat) ![Data Mining Badge](https://img.shields.io/badge/-Data%20Mining-00ACC1?style=flat) ![Machine Learning Badge](https://img.shields.io/badge/-Machine%20Learning-FF6F61?style=flat&logo=Python&logoColor=white)                                                                                                                                                                                                                                                               |
| **Cybersecurity**         | ![Cybersecurity Badge](https://img.shields.io/badge/-Cybersecurity-333333?style=flat)                                                                                                                                                                                                                                                                                                                                                                                                                      |
| **Design Tools**          | ![Canva](https://img.shields.io/badge/Canva-Design%20Graphics%20and%20More-red) [![Figma](https://img.shields.io/badge/Figma-Design%20Prototypes%20and%20Graphics-purple)](https://www.figma.com/)                                                                                                                                                                                                                                                                                                        |
| **Video Editing**         | [![VEED](https://img.shields.io/badge/VEED-Create%20and%20Edit%20Videos-blue)](https://www.veed.io/)                                                                                                                                                                                                                                                                                                                                                                                                       |
| **Content writing**       | [![Medium](https://img.shields.io/badge/Medium-Writing%20and%20Publishing-black)](https://medium.com/)                                                                                                                                                                                                                                                                                                                                                                                                       |
| **Problem Solving**       | ![Data Structures Badge](https://img.shields.io/badge/-Data%20Structures-008080?style=flat) ![Algorithms Badge](https://img.shields.io/badge/-Algorithms-00FFFF?style=flat) ![System Design Badge](https://img.shields.io/badge/-System%20Design-4682B4?style=flat) ![Quantitative Reasoning Badge](https://img.shields.io/badge/-Quantitative%20Reasoning-800080?style=flat)                                                                                                                                                                               |
| **Core Subjects**         | ![Computer Networks Badge](https://img.shields.io/badge/-Computer%20Networks-FFD700?style=flat) ![Operating Systems Badge](https://img.shields.io/badge/-Operating%20Systems-008000?style=flat) ![OOPS Badge](https://img.shields.io/badge/-OOPS-FF8C00?style=flat) ![DBMS Badge](https://img.shields.io/badge/-DBMS-4B0082?style=flat)                                                                                                                                                                                                                               |
| **Development OS**        | ![Windows Badge](https://img.shields.io/badge/-Windows-0078D6?style=flat&logo=Windows&logoColor=white) ![Ubuntu Badge](https://img.shields.io/badge/-Ubuntu-E95420?style=flat&logo=Ubuntu&logoColor=white)                                                                                                                                                                                                                                                                                                |

<h3 align="left">Connect with me:</h3>
<p align="left">
    <a href="https://twitter.com/NautiyalAwadesh" target="blank"><img align="center"
            src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Logo_of_Twitter.svg/1200px-Logo_of_Twitter.svg.png"
            alt="awadeshnautiyal" height="30" width="40" /></a>
    <a href="https://linkedin.com/in/awadesh-nautiyal" target="blank"><img align="center"
            src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/ca/LinkedIn_logo_initials.png/640px-LinkedIn_logo_initials.png"
            alt="awadesh-nautiyal" height="30" width="40" /></a>
    <a href="https://fb.com/100068354002459" target="blank"><img align="center"
            src="https://upload.wikimedia.org/wikipedia/en/thumb/0/04/Facebook_f_logo_%282021%29.svg/768px-Facebook_f_logo_%282021%29.svg.png?20210818083032"
            alt="100068354002459" height="30" width="40" /></a>
</p>

<p><img align="left"
        src="https://github-readme-stats.vercel.app/api/top-langs?username=awadesh365&show_icons=true&locale=en&layout=compact"
        alt="awadesh365" /></p>

<p>&nbsp;<img align="center"
        src="https://github-readme-stats.vercel.app/api?username=awadesh365&show_icons=true&locale=en"
        alt="awadesh365" /></p>

<p><img align="center" src="https://github-readme-streak-stats.herokuapp.com/?user=awadesh365&" alt="awadesh365" /></p>
